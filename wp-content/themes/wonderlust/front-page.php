<?php get_header(); ?>
<section id="content" role="main">
<?php
	$date = new DateTime($Date1);
	$timestamp = $date->getTimestamp();
	$args = array('post_type' => 'post', 'posts_per_page' => 5);
	if(get_field('header_slider')){
		$sliderPosts = get_field('header_slider');
		$args['posts_per_page'] = count($sliderPosts);
		$args['post__in'] = $sliderPosts;
		$args['orderby'] = 'post__in';
	} 
	$wp_query = new WP_Query( $args ); 
	if ( $wp_query->have_posts() ) : ?>
		<section class="header_slider">
			<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
				<div class="header_slide">
					<div class="header_slide_inner">
						<div class="header_slide_content">
							<?php is_portfolio($layoutPost); ?>
							<h2><a href="<?php the_permalink(); ?>"><?php echo wl_title($layoutPost); ?></a></h2>
							<div class="content_dek">
								<?php if(has_excerpt($layoutPost)){ ?>
									<a class="slider_dek" href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
								<?php } ?>
								<?php	
								if(!get_field('hide_author', $layoutPost)){
									if ( function_exists( 'coauthors_posts_links' ) ) {
									    $author = coauthors_posts_links(', ', ' and ', '<div class="post_author">By ', '</div>');
									} else {
									    the_author_posts_link();
									} 
								} ?>
							</div>
						</div>
						<div class="header_slide_image">
							<div class="header_slide_image_sizer">
							</div>
							<a href="<?php the_permalink(); ?>">
								<div class="header_slide_image_inner">
									<?php $slideImage = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ); ?>
									<div class="post_image_container bg_centered" style="background-image:url(<?php echo $slideImage; ?>);">
									</div>
<!-- 									<?php the_post_thumbnail('large'); ?> -->
								</div>
							</a>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</section>
		<script>
			var slickHeader = jQuery('.header_slider').slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				vertical: false,
		        infinite: true,
		        fade:true,
		        autoplay:true,
		        autoplaySpeed:3500,
		        arrows:false,
		        dots:true,
		        adaptiveHeight: true
			});
		</script>
		<?php
		wp_reset_query();
	endif;
	if( have_rows('homepage_layout', get_option('page_on_front')) ): ?>
		<section class="homepage_layout">
		    <?php while ( have_rows('homepage_layout', get_option('page_on_front')) ) : the_row();
		    	switch (get_row_layout()):
			    	case '3_square_images_with_hed':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
				    		<div class="inner_content">
				    			<section class="home_layout_section three_squares_with_hed">
					    			<?php foreach($layoutPosts as $layoutPost): ?>
						    			<div id="post_<?php echo $layoutPost; ?>" class="square_post">
								    		<div class="square_post_image">
									    		<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    		<div class="image_sizer">
										    		</div>
										    		<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost),'small-medium' ); ?>
										    		<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
											    		
										    		</div>
								    			</a>
							    			</div>
							    			<div class="square_post_hed post_thumb_hed">
								    			<?php is_video($layoutPost); ?>
								    			<?php is_portfolio($layoutPost); ?>
								    			<h2><a href="<?php echo get_the_permalink($layoutPost); ?>"><?php echo wl_title($layoutPost); ?></a></h2>
							    			</div>
						    			</div>
									<?php endforeach; ?>
				    			</section>
				    		</div>
						<?php endif;
			    	break;
			    	case '3_posts_with_color_hed':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
			    			<div class="inner_content">
				    			<section class="home_layout_section three_posts_with_color">
					    			<section class="three_posts_with_color_content">
						    			<?php foreach($layoutPosts as $layoutPost): ?>
							    			<div id="post_<?php echo $layoutPost; ?>" class="color_post">
								    			<div class="square_post_hed post_thumb_hed">
									    			<?php is_video($layoutPost); ?>
									    			<?php is_portfolio($layoutPost); ?>
									    			<h2>
										    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
											    			<?php echo wl_title($layoutPost); ?>
										    			</a>
										    		</h2>
									    			<?php if(has_excerpt( $layoutPost ) !== null){ ?>
									    				<div class="content_dek">
										    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
										    				</a>
									    				</div>
									    			<?php } ?>
								    			</div>
								    			<div class="square_post_image">
									    			<div class="image_sizer">
											    	</div>
											    	<div class="post_image_container">
												    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
									    					<?php echo get_the_post_thumbnail( $layoutPost, 'small-medium' ); ?>
												    	</a>
											    	</div>
								    			</div>
							    			</div>
										<?php endforeach; ?>
					    			</section>
					    			<?php if(!my_wp_is_mobile()){ ?>
						    			<section class="placeholder_300">
							    			<div id='DSK_MR'>
												<script src='https://www.googletagservices.com/tag/js/gpt.js'>
		googletag.pubads().definePassback('/72241422/Wonderlust/Standard/Xl_box/US_BRAYSISLAND_Q416Q317_XLBOX_DESK_GEO', [300, 600]).display();
												</script>
											</div>
							    		</section>
									<?php } ?>
				    			</section>
			    			</div>
			    			<?php if(my_wp_is_mobile()){ ?>
			    				<section class="placeholder_300">
				    				<!-- /21631492478/hotel_fauchere_300x250 -->
									<div id='div-gpt-ad-1510071482062-0' style='margin:0 auto; height:250px; width:300px;'>
										<script>
											googletag.cmd.push(function() { googletag.display('div-gpt-ad-1510071482062-0'); });
										</script>
									</div>
			    				</section>
			    			<?php } ?>
						<?php endif;
			    	break;
			    	case '4_posts_layout':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
			    			<div class="inner_content">
			    				<section class="home_layout_section four_posts_layout">
					    			<?php foreach($layoutPosts as $layoutPost): 
							    			$pType = get_post_type( $layoutPost ); ?>
							    			<div id="post_<?php echo $layoutPost; ?>" class="quarter_width_post pType_<?php echo $pType; ?>">
								    			<div class="post_thumb_hed">
									    			<div class="post_hed_sizer">
										    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    			</a>
									    			</div>
									    			<div class="post_thumb_hed_content">
										    			<?php is_video($layoutPost); ?>
										    			<?php is_portfolio($layoutPost); ?>
										    			<h2>
											    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
												    			<?php echo wl_title($layoutPost); ?>
											    			</a>
											    		</h2>
										    			<?php if(has_excerpt( $layoutPost ) && $pType == 'page'){ ?>
										    				<div class="content_dek">
											    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
											    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
											    				</a>
										    				</div>
										    			<?php } ?>
									    			</div>
								    			</div>
								    			<?php if(get_post_type( $layoutPost ) != 'page'){ ?>
									    			<div class="post_image_wrapper">
										    			<div class="image_sizer">
												    	</div>
												    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'small-medium' ); ?>
												    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
															<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
															</div>
												    	</a>
									    			</div>
									    			<?php if(has_excerpt( $layoutPost )){ ?>
									    				<div class="content_and_hed">
										    				<?php is_video($layoutPost); ?>
										    				<?php is_portfolio($layoutPost); ?>
										    				<h2 class="tablet_size">
												    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    			<?php echo wl_title($layoutPost); ?>
												    			</a>
												    		</h2>
										    				<div class="content_dek">
											    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
											    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
											    				</a>
										    				</div>
									    				</div>
									    			<?php } ?>
								    			<?php } ?>
							    			</div>
										<?php endforeach; ?>
			    				</section>
			    			</div>
						<?php endif;
			    	break;
			    	case 'ad_block':
			    		$adblock = get_sub_field('banner_ad');
			    		if($adblock): ?>
			    			<div class="ad_block">
				    			<?php if(!wp_is_mobile()){ ?>
					    			<div class="banner_ad_970">
					    				<div id='DSK_Leaderboard_2'>
											<script src='https://www.googletagservices.com/tag/js/gpt.js'>
  googletag.pubads().definePassback('/72241422/Wonderlust/Dashboard/US_GENESIS_Q3Q417_DASHBOARD_DESK_SB', [970, 250])
.display();
											</script>
										</div>
					    			</div>
				    			<?php }else{ ?>
				    				<!-- /21631492478/hotel_fauchere_300x50 -->
									<div class="mobile_leaderboard" id='div-gpt-ad-1509987508497-0' style='margin: 0 auto; height:50px; width:300px;'>
										<script>
										googletag.cmd.push(function() { googletag.display('div-gpt-ad-1510071482062-1'); });
										</script>
									</div>
				    			<?php } ?>
			    			</div>
			    		<?php
			    		endif;
			    	break;
			    	case 'city_layout':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts):
			    			foreach($layoutPosts as $layoutPost): ?>
				    			<section class="content_block_section city_layout">
								    <?php $cityID = $layoutPost; ?>
								    <?php $sections = get_field('post_sections', $cityID); ?>
								    <div class="inner_content">
										<div class="top_row">
											<div class="top_row_left">
												<div class="top_row_left_content">
													<div class="city_title">
														<?php if(get_field('associated_city', $cityID)){ ?>
															<h1><a href="<?php echo get_the_permalink($cityID); ?>"><?php echo get_field('associated_city', $cityID); ?></a></h1>
														<?php } ?>
														<h2><a href="<?php echo get_the_permalink($cityID); ?>"><?php echo get_the_title( $cityID ); ?></a></h2>
														<?php 
														if(has_excerpt( $cityID )){ ?>
															<div class="city_excerpt">
																<a href="<?php echo get_the_permalink($cityID); ?>">
																	<?php echo get_the_excerpt( $cityID ); ?>
																</a>
															</div>
														<?php
														} ?>
													</div>
													<div class="city_image_1">
														<?php $image1 = $sections[0]['post_section_image']['sizes']['small-medium']; ?>
														<a href="<?php echo get_the_permalink($cityID); ?>">
															<img src="<?php echo $image1; ?>"/>
														</a>
													</div>
												</div>
											</div>
											<div class="top_row_right">
												<a href="<?php echo get_the_permalink($cityID); ?>">
													<?php echo get_the_post_thumbnail( $cityID, 'small-medium' ); ?>
												</a>
											</div>
										</div>
										<div class="bottom_row">
											<div class="bottom_row_content">
												<?php 
												$args = array(
													'post_parent' => $cityID,
													'post_type'   => 'post', 
													'numberposts' => 1, 
												);
												$children = get_children( $args );
												if(count($children) > 0){
													foreach($children as $child){
														$cityChild = $child;
													} ?>
													<?php if(get_post_thumbnail_id( $cityChild->ID )){ ?>
														<div class="city_image_2">
															<?php $image2 = wp_get_attachment_image_url( get_post_thumbnail_id($cityChild->ID), 'small-medium' ) ?>
															<a href="<?php echo get_the_permalink($cityChild); ?>">
																<img src="<?php echo $image2; ?>"/>
															</a>
													</div>
													<?php } ?>
													<div class="extra_content">
														<a href="<?php echo get_the_permalink( $cityChild->ID ); ?>">
															<strong>PLUS</strong>
															<p><?php echo $cityChild->post_title; ?></p>
															<?php if(has_excerpt( $cityChild->ID )){ ?>
																<p><?php echo get_the_excerpt( $cityChild->ID ); ?></p>
															<?php } ?>
														</a>
													</div>
												<?php } ?>
<!--
												<?php if(get_field('extra_content', $cityID)){ ?>
													
												<?php } ?>
-->
											</div>
										</div>
								    </div>
								</section>
						<?php 
							endforeach;
						endif;
			    	break;
			    	case 'categories_layout':
			    		$layoutPosts = get_sub_field('layout_posts');
			    		if($layoutPosts): ?>
			    			<div class="inner_content">
				    			<section class="home_layout_section categories_layout">
					    			<?php foreach($layoutPosts as $layoutPost): 
						    			$cats = get_the_category( $layoutPost );
					    			?>
						    			<div id="post_<?php echo $layoutPost; ?>" class="category_post">
							    			<h3 class="cat_header_label"><?php echo $cats[0]->name; ?></h3>
							    			<div class="category_post_image">
								    			<div class="image_sizer">
										    	</div>
										    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'medium' ); ?>
										    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    		<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
										    		</div>
										    	</a>
							    			</div>
							    			<div class="category_post_hed post_thumb_hed">
								    			<?php is_video($layoutPost); ?>
								    			<?php is_portfolio($layoutPost); ?>
								    			<h2>
									    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
										    			<?php echo wl_title($layoutPost); ?>
									    			</a>
									    		</h2>
								    			<?php if(has_excerpt( $layoutPost ) !== null){ ?>
								    				<div class="content_dek">
									    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
									    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
									    				</a>
								    				</div>
								    			<?php } ?>
							    			</div>
						    			</div>
									<?php endforeach; ?>
				    			</section>
			    			</div>
						<?php endif;
			    	break;
			    	case 'content_block': ?>
			    		<div class="home_content_block">
				    		<div class="inner_content">
					    		<?php
					    		$headerLabel = get_sub_field('block_label');
					    		if(isset($headerLabel)){ ?>
					    			<div class="content_block_header">
						    			<div class="block_header_left block_line">
						    			</div>
						    			<div class="block_label">
							    			<?php echo get_sub_field('block_label'); ?>
						    			</div>
						    			<div class="block_right block_line">
						    			</div>
					    			</div>
						    	<?php
					    		} 
					    		if(get_sub_field('choose_row_layout')){ 
						    		while ( have_rows('choose_row_layout') ) : the_row();
						    			switch (get_row_layout()): 
							    			case 'single_post_layout':
							    				$layoutPosts = get_sub_field('layout_posts');
									    		if($layoutPosts): ?>
									    			<section class="content_block_section single_post_layout <?php echo get_sub_field('content_background'); ?>">
									    			<?php foreach($layoutPosts as $layoutPost): ?>
										    			<div id="post_<?php echo $layoutPost; ?>" class="<?php echo get_sub_field('post_layout'); ?>_container">
											    			<div class="single_post_layout_image post_image_wrapper">
												    			<div class="image_sizer">
														    	</div>
														    	<div class="post_image_container">
															    	<?php if(get_field('embedded_image', $layoutPost)){ ?>
																    	<?php echo get_field('embedded_image', $layoutPost); ?>
															    	<?php }else{ ?>
																    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    					<?php echo get_the_post_thumbnail( $layoutPost, 'large' ); ?>
																    	</a>
															    	<?php } ?>
														    	</div>
											    			</div>
											    			<div class="post_thumb_hed">
												    			<?php is_video($layoutPost); ?>
												    			<?php is_portfolio($layoutPost); ?>
												    			<h2>
													    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
														    			<?php echo wl_title($layoutPost); ?>
													    			</a>
													    		</h2>
												    			<?php if(has_excerpt( $layoutPost )){ ?>
												    				<div class="content_dek">
													    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
													    				</a>
												    				</div>
												    			<?php } ?>
												    			<?php if(get_field('hotel_name', $layoutPost)){ ?>
												    				<h6 class="hotel_name"><?php echo get_field('hotel_name', $layoutPost); ?></h6>
												    			<?php } ?>
												    			<?php if(get_field('extra_content', $layoutPost)){ ?>
												    				<div class="extra_content"><?php echo get_field('extra_content', $layoutPost); ?></div>
												    			<?php } ?>
											    			</div>
										    			</div>
													<?php endforeach; ?>
										    		</section>
												<?php endif;
							    			break;
							    			case 'two_posts_layout':
							    				$layoutPosts = get_sub_field('layout_posts');
									    		if($layoutPosts): ?>
									    			<section class="content_block_section two_posts_layout">
										    			<?php foreach($layoutPosts as $key=>$layoutPost): ?>
											    			<div id="post_<?php echo $layoutPost; ?>" class="two_posts_layout_<?php echo get_sub_field('2_post_layout'); ?> layout_<?php echo $key; ?>">
												    			<div class="post_image_wrapper">
													    			<div class="image_sizer">
															    	</div>
															    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'medium' ); ?>
																    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
																			<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
																			</div>
																    	</a>
												    			</div>
												    			<div class="post_thumb_hed<?php echo get_sub_field('yellow_block') ? ' yellow_bg' : ''; ?>">
													    			<?php is_video($layoutPost); ?>
													    			<?php is_portfolio($layoutPost); ?>
													    			<h3>
														    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
															    			<?php echo wl_title($layoutPost); ?>
															    			
														    			</a>
														    		</h3>
														    		<?php if(has_excerpt($layoutPost)){ ?>
														    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
																			<div class="content_dek">
																				<?php echo strip_tags(get_the_excerpt($layoutPost)); ?>
																			</div>
														    			</a>
																	<?php } ?>
																	<?php if(get_field('sponsored', $layoutPost) && get_field('sponsored_text', $layoutPost)){ ?>
																		<div class="sponsored_tag"><?php echo get_field('sponsored_text', $layoutPost); ?></div>
																	<?php } ?>
													    			
												    			</div>
											    			</div>
														<?php endforeach; ?>
										    		</section>
												<?php endif;
							    			break;
							    			case 'three_posts_layout':
							    				$layoutPosts = get_sub_field('layout_posts');
									    		if($layoutPosts): ?>
									    			<section class="content_block_section three_varied_posts">
									    			<?php foreach($layoutPosts as $layoutPost): ?>
										    			<div id="post_<?php echo $layoutPost; ?>" class="varied_post">
											    			<div class="varied_post_image post_image_wrapper">
												    			<div class="image_sizer">
														    	</div>
														    	<?php $postImage = wp_get_attachment_image_url( get_post_thumbnail_id($layoutPost), 'small-medium' ); ?>
														    	<a href="<?php echo get_the_permalink($layoutPost); ?>">
																	<div class="post_image_container bg_centered" style="background-image:url(<?php echo $postImage; ?>);">
																	</div>
														    	</a>
														    	
											    			</div>
											    			<div class="post_thumb_hed">
												    			<?php is_video($layoutPost); ?>
												    			<?php is_portfolio($layoutPost); ?>
												    			<h3>
													    			<a href="<?php echo get_the_permalink($layoutPost); ?>">
														    			<?php echo wl_title($layoutPost); ?>
													    			</a>
													    		</h3>
												    			<?php if(has_excerpt( $layoutPost )){ ?>
												    				<div class="content_dek">
													    				<a href="<?php echo get_the_permalink($layoutPost); ?>">
													    					<?php echo apply_filters('the_content', get_the_excerpt( $layoutPost )); ?>
													    				</a>
												    				</div>
												    			<?php } ?>
											    			</div>
										    			</div>
													<?php endforeach; ?>
										    		</section>
												<?php endif;
							    			break;
							    			default:
							    			break;
						    			endswitch;
						    		endwhile;
						    	} ?>
				    		</div>
			    		</div>
			    	<?php
			    	break;
		    	endswitch;
		    endwhile; ?>
		</section>
    <?php endif; ?>
</section>

<?php get_footer(); ?>