<?php get_header(); ?>
<?php $terms = get_the_terms( get_the_id(), 'post-type' ); ?>
<?php if($terms[0]->slug != 'photo-essay' && !wp_is_mobile()){ ?>
	<div id="trending_bar">
		<div id="trending_bar_content">
			<strong>TRENDING</strong>  Rome, Cuba, Paris, Mexico City, Quito, Copenhagen, Lapland
		</div>
	</div>
<?php } ?>
<div class="inner_content">
	<section id="content" role="main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			
			$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'small' );
			$link = get_the_permalink();
			?>
			<header id="single_header" class="<?php echo $image[1] < $image[2] ? 'port_img' : 'horz_img'; ?>">
				<div id="single_header_content">
					<?php if($terms[0]->slug != 'photo-essay' && (!get_field('video_embed') && $terms[0]->slug != 'video')){ ?>
						<?php 
						$featImage = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ); 
						$imageCaption = get_post(get_post_thumbnail_id())->post_excerpt;
						?>
						<div class="featured_thumbnail_container">
							<div class="featured_thumbnail_content">
								<div class="image_sizer"></div>
								<div class="featured_thumbnail bg_centered" style="background-image:url(<?php echo $featImage; ?>);">
								</div>
							</div>
							<?php
							if(isset($imageCaption) && has_post_thumbnail()){ ?>
								<div class="featured_image_caption">
									<?php echo $imageCaption; ?>
								</div>
							<?php
							} ?>
						</div>
						<?php
					} 
					if(get_field('video_embed') && $terms[0]->slug == 'video'){ ?>
						<div class="post_video">
							<?php echo get_field('video_embed'); ?>
						</div>
					<?php
					} ?>
					<div class="single_header_title_container">
						<div class="single_header_title">
							<?php if($terms[0]->slug == 'photo-essay'){ ?>
								<p class="cat_label">Portfolio</p>
							<?php } ?>
<!--
							<?php 
								if(get_field('sponsored')){
									if(get_field('sponsored_text')){ ?>
										<div class="sponsored_tag"><a href="https://ad.doubleclick.net/ddm/clk/403213100;203188449;o"><?php echo get_field('sponsored_text'); ?></a></div>
									<?php 
									} 
									$date = new DateTime($Date1);
									$timestamp = $date->getTimestamp();
									?>
									<img src="https://tag.researchnow.com/t/beacon?adn=3&ca=20053123&cr=creative&did=DEVICEID&ord=%n&pl=203188449&pr=11081&si=Nativly&rd=https://ad.doubleclick.net/ddm/ad/N553.1674088NATIV.LY/B20053123.203188449;sz=1x1;ord=<?php echo $timestamp; ?>;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=?" BORDER=0 WIDTH=1 HEIGHT=1 ALT="Advertisement">
									<SCRIPT TYPE="text/javascript" SRC="https://pixel.adsafeprotected.com/rjss/st/96908/17818057/skeleton.js"></SCRIPT>
									<NOSCRIPT><IMG SRC="https://pixel.adsafeprotected.com/rfw/st/96908/17818056/skeleton.gif" BORDER=0 WIDTH=1 HEIGHT=1 ALT=""></NOSCRIPT>
									<img src="http://secure.insightexpressai.com/adServer/adServerESI.aspx?script=false&bannerID=1761518&rnd=<?php echo $timestamp; ?>&redir=http://secure.insightexpressai.com/adserver/1pixel.gif">
									<?php
								}
							?>
-->
							<h1>
								<?php echo wl_title(); ?>
							</h1>
							<?php if(has_excerpt()){ ?>
								<div class="content_dek">
									<?php 
									the_excerpt(); ?>
								</div>
							<?php } ?>
							<?php if(!get_field('hide_author')){
										if ( function_exists( 'coauthors_posts_links' ) ) {
											$author = coauthors_posts_links(', ', ' and ', '<div class="post_author">By ', '</div>');
										}else{
											echo '<p>By '.get_the_author_posts_link().'</p>';
										}
									} ?>
						</div>
					</div>
				</div>
				
			</header>
			<div id="single_content_container">
				<div id="single_content">
					<div class="single_content_text">
					<?php // ECHO THE CONTENT FOR ALL POSTS
					the_content(); ?>
					</div>
					<?php
					if($terms[0]->slug != 'photo-essay'){
						
						get_social_share();
					}
					// TOP LIST
/*
						if( have_rows('top_list') ): ?>
							<div class="top_list_container">
								<ol class="top_list">
									<?php
									while ( have_rows('top_list') ) : the_row(); ?>
										<li>
											<h3 class="top_list_title"><?php the_sub_field('list_item_title'); ?></h3>
											<?php the_sub_field('list_item_content'); ?>
										</li>
									<?php
									endwhile; ?>
								</ol>
							</div>
						<?php 
						endif;
*/
					// END TOP LIST
					// PHOTO ESSAY
						if( have_rows('pe_images') ): ?>
							<div class="photo_essay_container">
								<?php
								while ( have_rows('pe_images') ) : the_row();
									$postTitle = get_sub_field('pe_title');
									$postSlug = implode('-', explode(' ', $postTitle)); ?>
									<a name="<?php echo $postSlug; ?>"></a>
									<div class="photo_essay">
										<div class="photo_essay_image">
											<?php $image = get_sub_field('pe_image'); ?>
											<img src="<?php echo $image['sizes']['large']; ?>"/>
										</div>
										<div class="photo_essay_content">
											
											<?php if(get_sub_field('pe_title')){ 
												$postTitle = get_sub_field('pe_title');
											?>
												<h3><?php the_sub_field('pe_title'); ?></h3>
											<?php } ?>
											<?php the_sub_field('pe_caption'); ?>
											<div class="photo_essay_social">
												<?php get_social_share(false, $postTitle, $link.'#'.$postSlug); ?>
											</div>
										</div>
									</div>
								<?php
								endwhile; ?>
							</div>
						<?php 
						endif;
					// END PHOTO ESSAY
					?>
					<?php if($terms[0]->slug == 'photo-essay'){ ?>
						<div id="trending_bar">
							<div id="trending_bar_content">
								<strong>TRENDING</strong>  Rome, Cuba, Paris, Mexico City, Quito, Copenhagen, Lapland
							</div>
						</div>
					<?php } ?>
					<footer class="footer">		
<!-- 						<?php get_related_posts(get_queried_object_id(), false, true, 3); ?> -->
					</footer>
				</div>
				<?php
				if($terms[0]->slug != 'photo-essay'){
					get_sidebar();
				} ?>
			</div>
		<?php
		endwhile; endif; ?>
		<?php if(wp_is_mobile()){ ?>
			<div id="trending_bar">
				<div id="trending_bar_content">
					<strong>TRENDING</strong>  Rome, Cuba, Paris, Mexico City, Quito, Copenhagen, Lapland
				</div>
			</div>
		<?php } ?>
	</section>
</div>
<script>
	$('.wp-caption-text, .featured_image_caption').each(function(){
		var credit = $(this).text();
		if (credit.indexOf('***') >= 0){
			credit = credit.split('***');
			console.log(credit);
			$('<p class="photo_credit">'+credit[1]+'</p>').insertBefore($(this));
			$(this).text(credit[0]);
			$(this).parent().addClass('has_photo_credit');
		}
	});
</script>
<?php get_footer(); ?>