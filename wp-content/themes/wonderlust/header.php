<?php 
	$seenAd = true;
	if(!isset($_COOKIE['ad_presented'])){ 
		$seenAd = false;
		setcookie('ad_presented', 1, time() + 86400, "/");
	} ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
<?php wp_head(); ?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-9382804221504320",
    enable_page_level_ads: true
  });
</script>
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>
<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/21631492478/hotel_fauchere_300x250', [300, 250], 'div-gpt-ad-1510071482062-0').addService(googletag.pubads());
    googletag.defineSlot('/21631492478/hotel_fauchere_300x50', [300, 50], 'div-gpt-ad-1510071482062-1').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
</head>
<body <?php body_class(); ?>>
	<div id="wrapper" class="hfeed">
		<?php if(!wp_is_mobile() && !my_wp_is_tablet()){ ?>
			<div id='DSK_Leaderboard' style="text-align:center;">
				<script src='https://www.googletagservices.com/tag/js/gpt.js'>
  googletag.pubads().definePassback('/72241422/Wonderlust/Dashboard/US_GENESIS_Q3Q417_DASHBOARD_DESK_SB', [970, 250])
.display();
				</script>
			</div>
		<?php }else if(my_wp_is_tablet()){ ?>
			<div id='DSK_Leaderboard' style="text-align:center;">
				<script src='https://www.googletagservices.com/tag/js/gpt.js'>
  googletag.pubads().definePassback('/72241422/Wonderlust/Standard/970x250_728x90/US_EMIRATES_Q216Q117_STANDARDBANNERS_DESK_RON_ADDEDVALUE_FLIGTH1', [728, 90])
.display();
				</script>
			</div>
		<?php } ?>
		<header id="header" role="banner">
			<div class="header_content">
				<section id="branding">
					<div id="site-title">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
							<img src="<?php echo get_template_directory_uri(); ?>/images/wl_logo.png"/>
						</a>
					</div>
					<div id="site-description"><?php bloginfo( 'description' ); ?></div>
				</section>
<!--
				<div id="search">
					<?php get_search_form(); ?>
				</div>
-->
				
				
				<div id="header_right">
<!--
					<div class="presented_by">
						<a href="<?php echo $amexUrl; ?>" target="_blank">Presented by <img src="<?php echo get_template_directory_uri(); ?>/images/amex.png"/></a>
					</div>
-->
					<div id="social_icons">
						<a class="social_share_button" rel="nofollow" target="_blank" href="https://www.facebook.com/thewonderlustlife" title="Find Us On Facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/fb.png" /></a>
							
							<a class="social_share_button" rel="nofollow" target="_blank" href="https://www.instagram.com/thewonderlustlife/" title="Follow Us On Instagram"><img src="<?php echo get_template_directory_uri(); ?>/images/insta.png" /></a>

					       <a class="social_share_button" target="_blank" rel="nofollow" href="https://twitter.com/lovewonderlust" title="Follow Us On Twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>
<!--
					       
					        <a class="social_share_button" target="_blank" rel="nofollow" href="http://pinterest.com/wonderlusttravel" title="Add Us On Pinterest"><img src="<?php echo get_template_directory_uri(); ?>/images/pin.png" /></a>
-->
					</div>
					<div id="nav_toggle">
						<div class="nav_toggle_line">
						</div>
						<div class="nav_toggle_line">
						</div>
						<div class="nav_toggle_line">
						</div>
					</div>
					<div id="nav_search">
						<div id="nav_search_container">
							<div id="search_toggle">
								<img class="mag_glass" src="<?php echo get_template_directory_uri(); ?>/images/search.png"/>
								<img class="close_search" src="<?php echo get_template_directory_uri(); ?>/images/search_x.png"/>
							</div>
							<?php get_search_form(); ?>
						</div>
					</div>
						
				</div>
			</div>
			<div class="menu_overlay">
				<div class="menu_overlay_content">
					<nav id="menu" role="navigation">
<!-- 						<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?> -->
						<?php $cats = get_categories(array('exclude' => array( 1 ))); ?>
						<ul>
							<?php foreach($cats as $cat){ 
								$args = array('category' => $cat->term_id, 'posts_per_page' => 1);
								$catPost = get_posts( $args );
							?>
								<li><a href="<?php echo get_permalink( $catPost[0]->ID ); ?>"><?php echo $cat->name; ?></a></li>
							<?php } ?>
						</ul>
					</nav>
					<div class="menu_overlay_bottom mobile">
						<div id="search">
							<?php get_search_form(); ?>
						</div>
						<div id="menu_social_icons">
							<a class="social_share_button" rel="nofollow" target="_blank" href="https://www.facebook.com/thewonderlustlife" title="Find Us On Facebook"><img src="<?php echo get_template_directory_uri(); ?>/images/fb.png" /></a>
							
							<a class="social_share_button" rel="nofollow" target="_blank" href="https://www.instagram.com/thewonderlustlife/" title="Follow Us On Instagram"><img src="<?php echo get_template_directory_uri(); ?>/images/insta.png" /></a>

					       <a class="social_share_button" target="_blank" rel="nofollow" href="https://twitter.com/lovewonderlust" title="Follow Us On Twitter"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>
					       
<!-- 					        <a class="social_share_button" target="_blank" rel="nofollow" href="http://pinterest.com/wonderlusttravel" title="Add Us On Pinterest"><img src="<?php echo get_template_directory_uri(); ?>/images/pin.png" /></a> -->

						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="container">