<?php get_header(); ?>
<section id="content" role="main">
	<header class="header">
		<div class="inner_content">
			<?php the_post(); ?>
			<h1 class="entry-title author"><?php the_author_link(); ?></h1>
			<div class="author_info">
				<?php if(get_field('profile_photo', 'user_'.get_the_author_meta( 'ID' ))){ 
					$authorImage = get_field('profile_photo', 'user_'.get_the_author_meta( 'ID' ));
				?>
					<div class="author_image_container">
						<div class="image_sizer">
						</div>
						<div class="author_image bg_centered" style="background-image:url(<?php echo $authorImage['sizes']['small']; ?>);">
						</div>
					</div>
				<?php } ?>
				<?php if(get_the_author_meta( 'user_description' )){ ?>
					<div class="author_description">
						<?php echo apply_filters( 'the_content', get_the_author_meta( 'user_description' )); ?>
					</div>
				<?php } ?>
			</div>
		</div>
	</header>
	<?php 
	$args = array(
	    'author'        =>  get_the_author_meta( 'ID' ),
	    'orderby'       =>  'rand',
	    'order'         =>  'ASC',
	    'posts_per_page' => 3
    );
	$author_query = new wp_query( $args );
	if( $author_query->have_posts() ): ?>
		<div class="inner_content">
		    <div class="related_posts author_posts">
			    <h2><span>More by</span> <?php echo get_the_author_meta( 'first_name' ); ?></h2>
			    <div class="related_posts_container">
			    	<?php
			        while( $author_query->have_posts() ): $author_query->the_post(); ?>
			        	<div class="related_post">
				            <div class="post_image_wrapper">
				    			<div class="image_sizer">
						    	</div>
						    	<a href="<?php echo get_the_permalink(); ?>">
							    	<?php
								    if(get_field('3x2_image')){
								    	$image = get_field('3x2_image');
								    	$image = $image['sizes']['small-medium'];
							    	}else if(has_post_thumbnail()){
								    	$image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'small-medium'); 
								    } ?>
							    	<div class="post_image_container bg_centered" style="background-image:url(<?php echo $image; ?>);">
							    	</div>
								</a>
			    			</div>
				            <div class="post_thumb_hed">
			                <h4><a href="<?php the_permalink()?>" title="<?php the_title(); ?>" rel="nofollow"><?php the_title(); ?>
				                <span>
				                	<?php echo wp_strip_all_tags( get_the_excerpt() ); ?>
				                </span>
			                </a></h4>
				            </div>
			            </div>
			        <?php endwhile;
			        wp_reset_postdata(); ?>
		    	</div>
		    </div>
		</div>
	<?php
	endif; ?>
	
</section>
<?php get_footer(); ?>