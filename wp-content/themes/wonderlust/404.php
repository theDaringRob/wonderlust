<?php get_header(); ?>
	<section id="content" role="main">
		<article id="post-0" class="post not-found">
			<header class="header">
				<div class="inner_content">
				<h1 class="entry-title"><?php _e( 'Looks like you might be lost!', 'blankslate' ); ?></h1>
				<div class="search_404 ">
					<p><?php _e( 'Try searching something else?', 'blankslate' ); ?></p>
					<?php get_search_form(); ?>
				</div>
				</div>
			</header>
			<section class="entry-content">
				<div class="inner_content">
					<?php get_related_posts(false, 'May we make a suggestion?', false, 3, array(16,14,10)); ?>
				</div>
			</section>
		</article>
	</section>
</div>
<?php get_footer(); ?>