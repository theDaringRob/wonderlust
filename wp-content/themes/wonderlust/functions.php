<?php

add_action('init', 'customRSS');

function customRSS(){
    add_feed('msn', 'msnRSSFunc');
}

function msnRSSFunc(){
	get_template_part('rss', 'msn');
}
	
function wl_title($id = false){
	if(!$id){
		global $post;
		$id = $post->ID;
	}
	if(get_field('formatted_title', $id)){ 
		$title = get_field('formatted_title', $id);	
	}else{
		$title = get_the_title( $id );
	}
	return $title;
}

add_action('registered_post_type', 'igy2411_make_posts_hierarchical', 10, 2 );

// Runs after each post type is registered
function igy2411_make_posts_hierarchical($post_type, $pto){

    // Return, if not post type posts
    if ($post_type != 'post') return;

    // access $wp_post_types global variable
    global $wp_post_types;

    // Set post type "post" to be hierarchical
    $wp_post_types['post']->hierarchical = 1;

    // Add page attributes to post backend
    // This adds the box to set up parent and menu order on edit posts.
    add_post_type_support( 'post', 'page-attributes' );

}

add_action(
  'admin_menu', function () {
    remove_meta_box('postexcerpt', 'post', 'normal');
  }, 999
);

add_action('edit_form_after_title', 'add_excerpt_below_title');

function add_excerpt_below_title($post){
	if($post->post_type == 'post'){
		echo '<h2 style="background-color:white; margin-top:10px; top:12px; position:relative; border:1px solid rgb(220,220,220);">Dek</h2>';
		post_excerpt_meta_box($post);
	}
}

function my_wp_is_mobile() {
	include_once ( get_template_directory() . '/Mobile-Detect-master/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	if( $detect->isMobile() && !$detect->isTablet() ) {
		return true;
	}else{
		return false;
	}
}

function my_wp_is_tablet() {
	include_once ( get_template_directory() . '/Mobile-Detect-master/Mobile_Detect.php');
		$detect = new Mobile_Detect;
	if( $detect->isTablet() ) {
		return true;
	}else{
		return false;
	}
}

add_action( 'after_setup_theme', 'vs_setup' );
function vs_setup(){
	load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_post_type_support( 'page', 'excerpt' );
	add_image_size( 'small', 200 );
	add_image_size( 'small-medium', 600 );
	add_image_size( 'medium', 900 );
	add_image_size( 'large', 1400 );	
	add_image_size( 'full', 1900 );
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 640;
	register_nav_menus(
	array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
	);
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	acf_add_options_page( array('page_title' => 'Footer') );
}

add_filter( 'body_class', function( $classes ) {
    if ( !wp_is_mobile() ) {
        array_push($classes, 'is_desktop');
    }else{
	    array_push($classes, 'is_mobile');
    }
    if (is_single() && get_the_terms($post->ID, 'post-type') ) {
	    global $post;
	    foreach(get_the_terms( $post->ID, 'post-type' ) as $term) {
	      $classes[] = 'post-type-'.$term->slug;
	    }
	  }
    return $classes;
} );

/*
add_filter( 'pre_option_upload_path', function( $upload_path ) {
    return '/wp-content/uploads';
});
*/

if (substr($_SERVER['REMOTE_ADDR'], 0, 4) == '127.'
        || $_SERVER['REMOTE_ADDR'] == '::1') {
    add_filter( 'pre_option_upload_url_path', function( $upload_url_path ) {
	    return 'http://wonderlusttravel.com/wp-content/uploads';
	});
}

function get_popular_posts($title = false){
	global $post;
/*
	$popArgs = array(
	    'post_type' => 'post',
	    'posts_per_page' => 5,
	    'post__not_in' => array($post->ID)
	);
*/
	$popOptions = get_field('top_stories', 'option');
	$popArray = array();
	foreach($popOptions as $pOption){
		if($pOption != $post->ID){
			array_push($popArray, $pOption);
		}
	}
	$popArgs = array('post_type' => 'post', 'posts_per_page' => count($popArray), 'orderby' => 'post__in', 'post__in' => $popArray);
	$pop_query = new wp_query( $popArgs );
	if( $pop_query->have_posts() ): ?>
	    <div class="pop_posts">
		    <?php if($title && $title != ''){ ?>
		    	<h3><?php echo $title; ?></h3>
		    <?php } ?>
		    <div class="pop_posts_container">
		    	<?php
		        while( $pop_query->have_posts() ):
		            $pop_query->the_post(); ?>
		            <div class="pop_post">
			            <div class="post_thumb_hed">
				            <a href="<?php the_permalink()?>" title="<?php the_title(); ?>" rel="nofollow">
					            <h5 style="margin-bottom:5px;"><?php echo wl_title(get_the_id()); ?></h5>
				                <?php if(has_excerpt()){ ?> 
					                <?php the_excerpt(); ?>
					            <?php } ?>
							</a>
			            </div>
		            </div>
		        <?php endwhile;
		        wp_reset_postdata(); ?>
	    	</div>
	    </div>
	<?php
	endif;
}

function get_related_posts($id=false, $title=false, $single=true, $count, $tags=false){
	if(!$id && $single){
		global $post;
		$id = $post->ID;
	}
	if($single && !$tags){
		$tags = wp_get_post_terms( $id, 'post_tag', array('fields' => 'ids') );
	}else{
		// get 3 random tags;
	}
	
	$tagArgs = array(
	    'post__not_in'        => array( $id ),
	    'posts_per_page'      => $count,
	    'ignore_sticky_posts' => 1,
	    'orderby'             => 'rand',
	    'tax_query' => array(
	        array(
	            'taxonomy' => 'post_tag',
	            'terms'    => $tags
	        )
	    )
	);
	$related_query = new wp_query( $tagArgs );
	if( $related_query->have_posts() ): ?>
	    <div class="related_posts">
		    <?php if($title && $title != ''){ ?>
		    	<h3><?php echo $title; ?></h3>
		    <?php } ?>
		    <div class="related_posts_container">
		    	<?php
		        while( $related_query->have_posts() ):
		            $related_query->the_post(); ?>
		            <div class="related_post">
			            <div class="post_image_wrapper">
			    			<div class="image_sizer">
					    	</div>
					    	<a href="<?php echo get_the_permalink(); ?>">
						    	<?php
							    if(has_post_thumbnail()){
							    	$image = wp_get_attachment_image_url( get_post_thumbnail_id(), 'small-medium'); 
							    }
							    if(get_field('3x2_image')){
							    	$image = get_field('3x2_image');
							    	$image = $image['sizes']['small-medium'];
						    	} ?>
						    	<div class="post_image_container bg_centered" style="background-image:url(<?php echo $image; ?>);">
						    	</div>
							</a>
		    			</div>
			            <div class="post_thumb_hed">
				            <a href="<?php the_permalink()?>" title="<?php the_title(); ?>" rel="nofollow">
				                <h4 style="margin-bottom:5px;">
					                <?php echo wl_title(get_the_id()); ?>
				                </h4>
			                	<div class="content_dek">
				                	<?php echo get_the_excerpt(); ?>
				                </div>
				            </a>
			            </div>
		            </div>
		        <?php endwhile;
		        wp_reset_postdata(); ?>
	    	</div>
	    </div>
	<?php
	endif;
}

function get_social_share($post = false, $postTitle = false, $postLink = false){
   	if(!$post){
		global $post;
	}
	$postID = $post->ID;
	if(!$postTitle){
		$postTitle = urlencode($post->post_title);
	}
	if(!$postLink){
		$postLink = urlencode(get_the_permalink($postID));
	}
	$emailMessage = strip_tags(get_field('email_share_message', 'option')); ?>
    <div class="social_share_container">
        <a class="social_share_button" rel="nofollow" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $postLink;?>&amp;t=<?php echo $postTitle; ?>" onclick="javascript:void window.open('http://www.facebook.com/sharer.php?u=<?php echo $postLink; ?>&amp;t=<?php echo $postTitle; ?>','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Share on Facebook."><img src="<?php echo get_template_directory_uri(); ?>/images/fb.png" /></a>

       <a class="social_share_button" target="_blank" rel="nofollow" href="<?php echo $postLink; ?>" onclick="javascript:void window.open('https://twitter.com/share?status=<?php echo $postLink; ?>&text=<?php echo $postTitle; ?>','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Tweet this!"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" /></a>

       
        <a class="social_share_button" target="_blank" rel="nofollow" href="<?php echo $postLink; ?>" onclick="javascript:void window.open('http://pinterest.com/pin/create/link/?url=<?php echo $postLink; ?>&description=<?php echo $postTitle; ?>','1410949501326','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;" title="Pin this!"><img src="<?php echo get_template_directory_uri(); ?>/images/pin.png" /></a>

        <a class="social_share_button" target="_self" rel="nofollow" href="mailto:?subject=Thought you might like this! &amp;body=<?php echo $emailMessage; ?>  %0D%0A %0D%0A<?php echo $postLink; ?>" title="Email this!"><img src="<?php echo get_template_directory_uri(); ?>/images/mail.png" /></a>
    </div>

		
<?php 
}

add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts(){
	wp_enqueue_script( 'jquery' );
	
/*
	wp_register_script( 'masonry', get_template_directory_uri()."/scripts/masonry.pkgd.min.js");
	wp_enqueue_script("masonry");
	
	wp_register_script( 'isotope', get_template_directory_uri()."/scripts/isotope.pkgd.min.js");
	wp_enqueue_script("isotope");
	
	wp_register_script( 'packery', get_template_directory_uri()."/scripts/packery-mode.pkgd.min.js");
	wp_enqueue_script("packery");
	
	wp_register_script( 'imagesloaded', get_template_directory_uri()."/scripts/imagesloaded.pkgd.min.js");
	wp_enqueue_script("imagesloaded");
	
	wp_register_script( 'cookie', get_template_directory_uri()."/scripts/js.cookie.js");
	wp_enqueue_script("cookie");
	
	wp_enqueue_style( 'jquery-ui-style',"https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css");

	wp_register_script( 'jquery-ui', "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js");
	wp_enqueue_script("jquery-ui");
*/
	
	wp_register_script( 'slick', "//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js");
	wp_enqueue_script("slick");
	
	wp_enqueue_style( 'slick-style',"//cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css");

	
	wp_register_script( 'scripts', get_template_directory_uri()."/scripts/scripts.js");
	wp_enqueue_script("scripts");
}

add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script(){
	if ( get_option( 'thread_comments' ) ) { 
		wp_enqueue_script( 'comment-reply' );	
	}
}

add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
	if ( $title == '' ) {
		return '&rarr;';
	} else {
		return $title;
	}
}

add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title ){
	return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init(){
	register_sidebar( array (
	'name' => __( 'Sidebar Widget Area', 'blankslate' ),
	'id' => 'primary-widget-area',
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => "</li>",
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
}

function blankslate_custom_pings( $comment ){
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
	<?php 
}

add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count ){
	if ( !is_admin() ) {
		global $id;
		$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
		return count( $comments_by_type['comment'] );
	} else {
		return $count;
	}
}

/*--------------------------------*\
	CUSTOM TAXONOMIES
\*--------------------------------*/

add_action( 'init', 'custom_taxonomy_items' );
// Register Custom Taxonomy
function custom_taxonomy_items()  {
	$labels = array(
	    'name'                       => 'Post Type',
	    'singular_name'              => 'Post Type',
	    'menu_name'                  => 'Post Types',
	    'all_items'                  => 'All Post Types',
	    'parent_item'                => 'Parent Post Type',
	    'parent_item_colon'          => 'Parent Post Type:',
	    'new_item_name'              => 'New Post Type Name',
	    'add_new_item'               => 'Add New Post Type',
	    'edit_item'                  => 'Edit Post Type',
	    'update_item'                => 'Update Post Type',
	    'separate_items_with_commas' => 'Separate Post Types with commas',
	    'search_items'               => 'Search Post Types',
	    'add_or_remove_items'        => 'Add or remove Post Types',
	    'choose_from_most_used'      => 'Choose from the most used Post Types',
	);
	$args = array(
	    'labels'                     => $labels,
	    'hierarchical'               => true,
	    'public'                     => true,
	    'show_ui'                    => true,
	    'show_admin_column'          => true,
	    'show_in_nav_menus'          => true,
	    'show_tagcloud'              => true,
	);
	register_taxonomy( 'post-type', array('post'), $args );
	register_taxonomy_for_object_type( 'post-type', array('post') );

}

/*--------------------------------*\
	CUSTOM POST TYPES
\*--------------------------------*/

function register_custom_post_types() {
	
	$labels = array(
        'name' => _x('Knowledge Base', 'post type general name'),
        'singular_name' => _x('Knowledge Base', 'post type singular name'),
        'add_new' => _x('Add New', 'kb'),
        'add_new_item' => __('Add New Knowledge Base Entry'),
        'edit_item' => __('Edit Knowledge Base Entry'),
        'new_item' => __('New Knowledge Base Entry'),
        'view_item' => __('View Knowledge Base Entry'),
        'search_items' => __('Search Knowledge Base Entries'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'kb', 'with_front' => false),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 5,
        'has_archive' => true,
        'supports' => array('title','editor','thumbnail','excerpt'),
        'taxonomies' => array('category',  'post_tag'),
      );
      
    register_post_type( 'kb' , $args );

}

//add_action('init', 'register_custom_post_types');

add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
	
	$_SESSION['displayedPosts'] = array();
}

add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
    switch($_REQUEST['fn']){
	case 'get_next_posts':
		$catID = $_REQUEST['catid'];
       $output = get_posts_grid(array('posts_per_page' => 14, 'post_status' => array('public','publish'), 'post_type' => 'post', 'cat' => $catID, 'paged' => intval($_REQUEST['paged']), 'orderby' => 'date', 'order' => 'DESC'),false);
        echo $output;
    break;
    
	default:
		$output = 'nothing here';
		echo $output;
	break;
	}
	die();
}

function is_video($id){
	$ptype = get_the_terms( $id, 'post-type' );
	if($ptype[0]->slug == 'video'){ ?>
		<p class="cat_label">
    		video
		</p>
	<?php
	}else{
		
	}
	return;
}

function is_portfolio($id){
	$ptype = get_the_terms( $id, 'post-type' );
	if($ptype[0]->slug == 'photo-essay'){ ?>
		<p class="cat_label">
    		portfolio
		</p>
	<?php
	}else{
		
	}
	return;
}