<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : ?>
		<header class="header">
			<div class="inner_content">
				<h1 class="entry-title"><?php printf( __( 'Here\'s what we found about <span>"%s"</span>', 'blankslate' ), get_search_query() ); ?></h1>
			</div>
		</header>
		<div class="inner_content search_inner">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'entry' ); ?>
			<?php endwhile; ?>
			<?php get_template_part( 'nav', 'below' ); ?>
		</div>
	<?php else : ?>
		<article id="post-0" class="post no-results not-found">
			<header class="header">
				<div class="inner_content">
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
				</div>
			</header>
			<section class="entry-content">
				<div class="inner_content search_inner">
					<p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
					<?php get_search_form(); ?>
				</div>
			</section>
		</article>
	<?php endif; ?>
</section>
<?php get_footer(); ?>