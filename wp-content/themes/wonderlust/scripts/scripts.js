var $ = jQuery.noConflict();

$(document).ready(function(){
	$('#nav_toggle').on('click', function(){
		$('.menu_overlay').slideToggle();
	});
	
	$('#search_toggle').on('click', function(){
		$('#nav_search').toggleClass('active');
		$('#s').focus();
	});
});

